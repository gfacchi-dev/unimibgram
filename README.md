﻿# unimibgram - SD 2020

- **Giuseppe Facchi mat. 845662**
- **Ruben Agazzi mat. 844736**


### Setup
-------------
#### Visual Studio 2019
1. Aprire unimibgram.sln
2. Avviare applicazione mediante preferibilmente su IIS Express

#### Visual Studio per Mac
1. Aprire unimibgram.sln
2. Avviare applicazione

#### Senza Visual Studio
1. Assicurarsi di aver installato il [runtime .NET Core](https://dotnet.microsoft.com/download "runtime .NET Core")
2. `$ dotnet restore` dalla cartella del progetto unimibgram (aprire il terminale dentro la cartella)
3. `$ dotnet run` dalla cartella del progetto unimibgram (aprire il terminale dentro la cartella)
4. (opzionale) `dotnet publish --configuration Release` per generare i file release dell'applicazione ed eseguire `$ dotnet run` dalla cartella in cui è stata effettuata la Publish



### Come Funziona
-------------
L'applicazione realizzata con l'aiuto della libreria SignalR ha il compito di far comunicare in real-time più utenti previa **registrazione**. L'interfaccia prevede:
- Una sezione relativa ai contatti, i quali potranno essere aggiunti mediante apposito input e button
- Dopo aver aggiunto un contatto l'utente dovrà clickare su un contatto per poter comunicare con lui
- Una singola chat dove l'utente vedrà recapitarsi i messaggi spediti a lui stesso
- Un input per inviare messaggi di testo all'utente selezionato precedentemente

### Librerie esterne utilizzate

- [Bootstrap v4](https://getbootstrap.com/ "Bootstrap v4")
- [JQuery](https://jquery.com/ "JQuery")
- [Unobstrusive-JQuery](https://github.com/aspnet/jquery-validation-unobtrusive "Unobstrusive-JQuery")
- [Entity Framework Core](https://docs.microsoft.com/it-it/ef/core/ "Entity Framework Core")
- [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr "SignalR")
- [Identity](https://docs.microsoft.com/it-it/aspnet/core/security/authentication/identity "Identity") (Senza customizzazioni grafiche)
- [Pomelo.EntityFrameworkCore.Mysql](https://www.nuget.org/packages/Pomelo.EntityFrameworkCore.MySql)