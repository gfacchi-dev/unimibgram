﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using unimibgram.Models;

namespace unimibgram.Data
{
    /// <summary>
    /// Classe: ApplicationDbContext
    /// Scopi: Gestire la creazione del modello dei dati e impostare correttamente il funzionamento di Entity Framework
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Message>()
                .HasOne(a => a.Sender)
                .WithMany(m => m.Messages)
                .HasForeignKey(u => u.UserID);

            builder.Entity<Message>()
                .HasOne(a => a.Receiver);

            builder.Entity<Contact>()
                .HasOne(a => a.ContactFrom)
                .WithMany(m => m.Contacts)
                .HasForeignKey(u => u.ContactFromID);

            builder.Entity<Contact>()
                .HasOne(a => a.ContactTo);

        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Contact> Contacts { get; set; }
    }
}
