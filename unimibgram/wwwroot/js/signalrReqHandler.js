﻿/*
 Questo script si occupa di comunicare con l'hub signalR per lo scambio dei messaggi via web socket
 */
const connection = new signalR.HubConnectionBuilder()
    .withUrl("/Home/Index")
    .withAutomaticReconnect()
    .configureLogging(signalR.LogLevel.Information)
    .build();

async function start() {
    try {
        await connection.start();
        console.log("connected");
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};

connection.onclose(async () => {
    await start();
});

start();

function sendMessageToHub(message) {
    connection.invoke('SendMessageTo', message)
        .catch(err => function () {
            console.error(err);
            return;
        });
    addMessageToChat(message);
}

connection.on('receiveMessage', addMessageToChat);