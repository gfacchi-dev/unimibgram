﻿/*
 Questo script si occupa di gestire la parte grafica dell'invio dei messaggi e anche della comunicazione con le API per aggiungere il messaggio.
 Lo script si occupa anche di aggiornare la visualizzazione dei messaggi
 */
class Message {
    constructor(username, text, receiverEmail) {
        this.username = username;
        this.text = text;
        this.receiverEmail = receiverEmail;
    }
}

const messagesQueue = [];

const username = userName;
const textInput = document.getElementById('messageText');
const receiverInput = document.getElementById('receiverEmail');
const chat = document.getElementById('chat');


$('#submitButton').click(function () {
    if (receiverInput.value === '') {
        alert('Selezionare utente a cui inviare il messaggio');
        return;
    } else if (receiverInput.value.trim() === "") {
        alert('Inserire il testo');
        return;
    } else {
        $.ajax({
            type: "POST",
            url: '/Home/Create',
            data: { username: username, text: textInput.value, receiverEmail: receiverInput.value },
            success: function (data) {
                messagesQueue.push(textInput.value);
                sendMessageTo();
                textInput.value = '';
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    }
    
});

function sendMessageTo() { 
    if (receiverInput.value.trim() === "") {
        alert("Errore: email vuota");
        return;
    } else {
        let text = messagesQueue.shift() || "";
        let receiverEmail = receiverInput.value;        
        let message = new Message(username, text, receiverEmail);
        sendMessageToHub(message);
    }
}

function addMessageToChat(message) {
    let isCurrentUserMessage = message.username === username;

    let row = document.createElement('div');
    row.className = "row";
    let offset = document.createElement('div');
    offset.className = isCurrentUserMessage ? "col-md-6 offset-md-6" : "";
    let container = document.createElement('div');
    container.className = isCurrentUserMessage ? "container darker" : "container";

    let sender = document.createElement('p');
    sender.className = isCurrentUserMessage ? "sender text-right text-white" : "sender text-left";
    sender.innerHTML = `From: ${message.username} <br> To: ${message.receiverEmail}`;

    let text = document.createElement('p');
    text.className = isCurrentUserMessage ? "text-right text-white" : "text-left";
    text.innerHTML = message.text;

    let when = document.createElement('p');
    when.className = isCurrentUserMessage ? "time-right text-light" : "time-left";

    var currentdate = new Date();
    when.innerHTML =
        currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + " "
        + currentdate.toLocaleString('it-IT', { hour: 'numeric', minute: 'numeric' });

    container.appendChild(sender);
    container.appendChild(text);
    container.appendChild(when);
    offset.appendChild(container);
    row.appendChild(offset);
    chat.appendChild(row);
}
