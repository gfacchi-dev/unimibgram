﻿/*
 Funzione delegata ad aggiungere un nuovo contatto dell'utente.
 Funzione asincrona con ajax, si occupa di aggiornare la lista dei contatti
 */
$("#aggiungiContatto").click(function (e) {

    e.preventDefault(); 
    var form = $(this);
    var url = form.attr('action');
    var email = $('#emailToAdd').val();
    $.ajax({
        type: "POST",
        url: '/Home/AddToContacts',
        data: {emailToAdd: email},
        success: function (data) {
            if (data === 'error') {
                alert('Errore: Utente non esistente');
            } else {
                let div = document.createElement('div');
                div.className = 'col-md-12 card';
                let cardBodyDiv = document.createElement('div');
                cardBodyDiv.className = 'card-body';
                let title = document.createElement('h5');
                title.className = "card-title";
                title.innerHTML = data;
                cardBodyDiv.appendChild(title);
                div.appendChild(cardBodyDiv);
                document.getElementById('book').appendChild(div);


                $('.card').click(function () {
                    $('.card').removeClass('card-active text-white shadow-lg');
                    $(this).addClass('card-active text-white shadow-lg');
                    var mail = $(this).children('.card-body').children(".card-title").html();
                    $('#receiverEmail').val(mail);
                    $('#with').html(mail);
                });

                $('.card').hover(
                    function () {
                        $(this).addClass('shadow-lg').css('cursor', 'pointer');
                    }, function () {
                        $(this).removeClass('shadow-lg');
                    }
                );
            }
           
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        }
    });


});

/*
 Funzione delegata a settare il contatto a cui mandare i mesasggi una volta cliccato
 */
$('.card').click(function () {

    $('.card').removeClass('card-active text-white shadow-lg');
    $(this).addClass('card-active text-white shadow-lg');
    var mail = $(this).children('.card-body').children(".card-title").html();
    $('#receiverEmail').val(mail);
    $('#with').html(mail);
});

$('.card').hover(
    function () {
        $(this).addClass('shadow-lg').css('cursor', 'pointer');
    }, function () {
        $(this).removeClass('shadow-lg');
    }
);