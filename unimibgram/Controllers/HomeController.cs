﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using unimibgram.Data;
using unimibgram.Models;

/// <summary>
/// Classe: HomeController
/// Scopi: Gestire la richiesta della pagina web dai client e restituire l'home page con i relativi componenti
/// Metodi: - GET   Index         => Resistuisce la vista corrispondente dopo il retrieve dei dati (messaggi, utente corrente e relativi contatti)
///         - POST  Create        => Crea il messaggio e lo aggiunge al contesto, EF avrà il compito di inserirlo nel DB
///         - POST  AddToContacts => Controlla se la mail del contatto da aggiungere esista nel context e, se non lo è, aggiunge la nuova istanza del contatto al context
///         - GET   Privacy       => Resistuisce la vista relativa al trattamento dei dati personali
///         - GET   Error         => Ha il compito di gestire gli errori e restituire la vista corrispondente quando necessario
/// </summary>

namespace unimibgram.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUser> _userManager;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context, UserManager<AppUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var currentUser = await _userManager.GetUserAsync(User);           
            var messages = await _context.Messages
                .Where(m => m.ReceiverID == currentUser.Id || m.UserID == currentUser.Id)
                .ToListAsync();
            var viewModel = new IndexViewModel();
            if(User.Identity.IsAuthenticated)
                viewModel.CurrentUsername = currentUser.UserName;
            viewModel.Messages = messages;
            var contacts = await _context.Contacts
                .Where(c => c.ContactFromID == currentUser.Id)
                .ToListAsync();
            var listaEmail = new List<string>();
            foreach(var contact in contacts)
            {
                var user = await _userManager.FindByIdAsync(contact.ContactToID);
                listaEmail.Add(user.Email);
            }
            
            viewModel.Contacts = listaEmail;
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(string username, string text, string receiverEmail)
        {        
            if (ModelState.IsValid)
            {
                var message = new Message
                {
                    Text = text,
                    Username = User.Identity.Name,
                    ReceiverEmail = receiverEmail
                };
                var sender = await _userManager.GetUserAsync(User);
                message.UserID = sender.Id;
                var receiver = await _userManager.FindByEmailAsync(receiverEmail);
                if (receiver != null)
                    message.ReceiverID = receiver.Id;
                message.TimeStamp = DateTime.Now;

                await _context.Messages.AddAsync(message);
                await _context.SaveChangesAsync();
                return Ok();
            }      
            return Error();
        }

        public async Task<String> AddToContacts(string emailToAdd)
        {
            if(ModelState.IsValid)
            {
                var currentUser = await _userManager.GetUserAsync(User);
                var userToAdd = await _userManager.FindByEmailAsync(emailToAdd);
                
                if(userToAdd != null)
                {
                    var contacts = await _context.Contacts
                        .Where(m => m.ContactFromID == currentUser.Id)
                        .ToListAsync();
                    if(!contacts.Select(i => i.ContactToID).Contains(userToAdd.Id))
                    {
                        await _context.Contacts.AddAsync(new Contact() 
                        { 
                            ContactFromID = currentUser.Id, 
                            ContactToID = userToAdd.Id 
                        });
                        await _context.SaveChangesAsync();
                        var contact = await _userManager.FindByEmailAsync(emailToAdd);
                        
                        return contact.Email;
                    }
                }
            }
            return "error";
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
