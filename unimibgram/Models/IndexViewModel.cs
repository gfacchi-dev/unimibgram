﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


/// <summary>
/// Classe: IndexViewModel
/// Scopi: Descrivere il modello dati utilizzato nel metodo Index (HomeController) che sarà utilizzato dalla vista corrispondente
/// </summary>

namespace unimibgram.Models
{
    public class IndexViewModel
    {
        public string CurrentUsername { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<String> Contacts { get; set; }
    }
}
