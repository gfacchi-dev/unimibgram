﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Classe: AppUser
/// Scopi: Descrive il modello dati dell'utente dell'applicazione, ereditando da IdentityUser
/// </summary>

namespace unimibgram.Models
{
    public class AppUser : IdentityUser
    {
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }

        public AppUser()
        {
            Messages = new HashSet<Message>();
            Contacts = new HashSet<Contact>();
        }
    }
}
