﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Classe: AppUser
/// Scopi: Descrivere il modello dati del contatto: prevede la definizione di "chi (contactfrom) ha aggiunto chi (contactto)"
/// </summary>

namespace unimibgram.Models
{
    public class Contact
    {
        public int Id { get; set; }

        public string ContactFromID { get; set; }
        public virtual AppUser ContactFrom { get; set; }

        public string ContactToID { get; set; }
        public virtual AppUser ContactTo { get; set; }
    }
}
