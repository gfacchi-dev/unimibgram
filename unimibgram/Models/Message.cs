﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Classe: Message
/// Scopi: Descrivere il modello dati del messaggio"
/// </summary>

namespace unimibgram.Models
{
    public class Message
    {
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string ReceiverEmail { get; set; }

        public DateTime TimeStamp { get; set; }

        public string UserID { get; set; }
        public virtual AppUser Sender { get; set; }

        public string ReceiverID { get; set; }
        public virtual AppUser Receiver { get; set; }
        
        
    }
}
