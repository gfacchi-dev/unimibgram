﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using unimibgram.Data;
using unimibgram.Models;

/// <summary>
/// Classe: ChatHub
/// Scopi: Gestire lo smistamento dei messaggi mediante utilizzo della libreria Microsoft.AspNetCore.SignalR e del manager utenti
/// Metodi: - SendMessageTo => Dato un messaggio contenente i dati relativi al messaggio stesso e alla mail del ricevente trova il suo utente corrispondente e asincronamente invia il messaggio, facendo eseguire la funzione "receiveMessage" al client
/// </summary>


namespace unimibgram.Hubs
{
    public class ChatHub : Hub
    {
        private readonly UserManager<AppUser> _userManager;

        public ChatHub(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task SendMessageTo(Message message)
        {
            var receiver = await _userManager.FindByEmailAsync(message.ReceiverEmail);
            await Clients.User(receiver.Id).SendAsync("receiveMessage", message);
        }
    }
}
